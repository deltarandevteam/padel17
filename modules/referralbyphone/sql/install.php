<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'referralbyphone` (
            `id_referralbyphone` INT UNSIGNED NOT null AUTO_INCREMENT,
            `id_sponsor` INT UNSIGNED NOT null,
            `email` VARCHAR(255) NOT null,
            `lastname` VARCHAR(128) NOT null,
            `firstname` VARCHAR(128) NOT null,
            `id_customer` INT UNSIGNED DEFAULT null,
            `id_cart_rule` INT UNSIGNED DEFAULT null,
            `id_cart_rule_sponsor` INT UNSIGNED DEFAULT null,
            `date_add` DATETIME NOT null,
            `date_upd` DATETIME NOT null,
            PRIMARY KEY (`id_referralbyphone`),
            UNIQUE KEY `index_unique_referralbyphone_email` (`email`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ws_ref_coupon_rule` (
            `id_ws_coupon_rule` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
			`id_coupon_rule` INT( 10 ) UNSIGNED NOT NULL ,
			`id_ws_sponsor` INT( 10 ) UNSIGNED NOT NULL ,
			`v_bonus` decimal(17,2) UNSIGNED NOT NULL,
			`reward_type` tinyint(1) NOT NULL DEFAULT "0",
			`reward_value` decimal(17,2) UNSIGNED NOT NULL,
			`code` VARCHAR(12) NULL DEFAULT \'0\',
            `cart_min_amount` decimal(17,2) UNSIGNED NOT NULL,
			`coupon_qty` INT( 11 ) UNSIGNED NOT NULL,
            `from_date` DATE NOT NULL,
            `to_date` DATE NOT NULL,
            `active` TINYINT( 1 ) UNSIGNED,
            `title_rule` VARCHAR(100) NULL DEFAULT \'0\',
            PRIMARY KEY (`id_ws_coupon_rule`)
        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
