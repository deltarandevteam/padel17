{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='Referral Program Plus' mod='referralbyphone'}</h3>
	<p>
        {l s='Welcome and thank you for purchasing the module.' mod='referralbyphone'}
    </p> 
	<p>
		{l s='This module will boost your sales!' mod='referralbyphone'}
	</p>
</div>
<div class="productTabs col-lg-12 col-md-3 panel">
    <div class="list-group">
        <ul class="nav nav-pills" id="navtabs16">
            <li class="active"><a href="#configForm" data-toggle="tab" class="list-group-item"><i class="icon-cogs"></i>&nbsp;{l s='Configuration' mod='referralbyphone'}</a></li>
            <li class=""><a href="#comfigCoupons" data-toggle="tab" class="list-group-item"><i class="icon-diamond"></i>&nbsp;{l s='Vouchers settings' mod='referralbyphone'}</a></li>
            <li class=""><a href="#comfigLoyalty" data-toggle="tab" class="list-group-item"><i class="icon-diamond"></i>&nbsp;{l s='Referral points' mod='referralbyphone'}</a></li>
            <li class=""><a href="#comfigEmails" data-toggle="tab" class="list-group-item"><i class="icon-envelope"></i>&nbsp;{l s='Emails settings' mod='referralbyphone'}</a></li>
            <li class=""><a href="#statSponsor" data-toggle="tab" class="list-group-item"><i class="icon-users"></i>&nbsp;{l s='Statistics' mod='referralbyphone'}</a></li>
        </ul>
    </div>
</div>